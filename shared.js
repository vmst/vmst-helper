//@ts-check
const lib_conv = require('viva-convert')

exports.quote = quote

/**
 * @param {string} val
 * @param {boolean} [no_bracket]
 */
function quote(val, no_bracket) {
    if (lib_conv.isAbsent(val)) return 'NULL'
    if (no_bracket === true) {
        return lib_conv.replaceAll(lib_conv.border_del(lib_conv.toString(val, ''),'[',']'), "'", "''")
    }
    return lib_conv.replaceAll(lib_conv.toString(val, ''), "'", "''")
}
