//@ts-check
const lib_conv = require('viva-convert')
const lib_os = require('os')
const quote = require('../shared').quote
const descr = require('./sch_description.js')

exports.go = go

/**
 * create or update schema
 * @param {string} name
 * @param {string} [description]
 * @returns {string}
 */
function go (name, description) {

    let query = [
        lib_conv.format("IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '{0}') BEGIN", quote(name, true)),
        lib_conv.format("   EXEC('CREATE SCHEMA [{0}]')", quote(name, true)),
        "END",
        descr.go(name, undefined, undefined, description)
    ]

    return query.join(lib_os.EOL)
}