//@ts-check
const lib_conv = require('viva-convert')
const lib_os = require('os')

exports.go = go

/**
 * exec sp_getapplock
 * @param {string} key name lock
 * @param {number} wait in msec, wait for free key, if 0, without wait
 * @param {string} [database] name database
 * @returns {string}
 */
function go (key, wait, database) {
    let key_beauty = lib_conv.toString(key, '')
    if (key_beauty.length <= 0) return undefined

    let wait_beauty = lib_conv.toInt(wait, -1)
    if (wait_beauty < 0) return undefined

    let exec_path = 'sp_getapplock'

    let database_beauty = lib_conv.toString(database, '')
    if (database_beauty.length > 0) {
        exec_path = lib_conv.border_add(database_beauty, '[', ']').concat('..', exec_path)
    }

    let error_text = lib_conv.format('Timed out wait sp_getapplock with key "{0}" and wait time {1} msec', [key_beauty, wait_beauty])
    if (!lib_conv.isEmpty(database_beauty)) {
        error_text = error_text.concat(' in database ', database_beauty)
    }

    return [
        "DECLARE @result INT",
        lib_conv.format("EXEC @result={0} @Resource='{1}', @LockOwner='Session', @LockMode='exclusive', @LockTimeOut={2}", [exec_path, key_beauty, wait_beauty]),
        "IF @result < 0 BEGIN",
        lib_conv.format("    RAISERROR('{0}', 16, 1)", error_text),
        "    RETURN",
        "END",
    ].join(lib_os.EOL)

}