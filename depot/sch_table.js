//@ts-check
const lib_conv = require('viva-convert')
const lib_os = require('os')
const descr = require('./sch_description.js')
const type = require('../@type.js')
const quote = require('../shared').quote

exports.go = go

/**
 * @typedef {Object} type_column
 * @property {string} name
 * @property {type.sqltype} type
 * @property {number|'max'} [len_chars]
 * @property {number} [precision]
 * @property {number} [scale]
 * @property {boolean} [nullable]
 * @property {boolean} [identity]
 * @property {number} [pk_position]
 * @property {string} [description]
 */

/**
 * create table or update table columns
 * @param {string} schema
 * @param {string} name
 * @param {string} [description]
 * @param {type_column[]} column_list
 * @param {'ignore'|'error'} exist_unknown_field
 * @returns {string}
 */
function go (schema, name, description, column_list, exist_unknown_field) {

    //if (!lib_conv.isEmpty())

    let pk = query_pk(schema, name, column_list)

    let pk_column_list = column_list.filter(f => !lib_conv.isEmpty(f.pk_position)).map(m => { return lib_conv.format("SELECT '{0}' n, {1} p", [quote(m.name,true), m.pk_position])}).join(" UNION ALL ")
    let all_column_list = column_list.map(m => { return lib_conv.format("SELECT '{0}' n", [quote(m.name,true), m.pk_position])}).join(" UNION ALL ")

    let query = [
        "IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '{0}' AND TABLE_NAME = '{1}') BEGIN",
        "   EXEC ('",
        "       CREATE TABLE [{0}].[{1}] (",
        column_list.map(m => { return '            '.concat(query_column(m)) }).join(','.concat(lib_os.EOL)).concat(lib_conv.isEmpty(pk) ? '' : ','),
        (lib_conv.isEmpty(pk) ? '' : '            '.concat(pk)),
        "       )".concat(column_list.some(f => f.len_chars === 'max') ? ' ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]' : ''),
        "   ')",
        "END ELSE BEGIN",
        (exist_unknown_field !== 'error' ? '' : [
            "    IF EXISTS (",
            "        SELECT * FROM(",
            "            SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.[COLUMNS] WHERE TABLE_SCHEMA = '{0}' AND TABLE_NAME = '{1}'",
            "        ) curr_column",
            "        LEFT JOIN (",
            "            ".concat(all_column_list),
            "        ) need_column ON curr_column.COLUMN_NAME = need_column.n",
            "        WHERE need_column.n IS NULL",
            "    ) BEGIN",
            "        RAISERROR('in table {0}.{1} existing columns does not match the expected', 16, 1)",
            "        RETURN",
            "    END",
        ].join(lib_os.EOL)),
        (lib_conv.isEmpty(pk_column_list) ? '' : [
            "    IF EXISTS (",
            "        SELECT * FROM (",
            "            SELECT kcu.COLUMN_NAME, kcu.ORDINAL_POSITION",
            "            FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc",
            "            JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu ON kcu.TABLE_SCHEMA =  tc.TABLE_SCHEMA AND kcu.TABLE_NAME =  tc.TABLE_NAME",
            "            WHERE tc.TABLE_SCHEMA = '{0}' AND tc.TABLE_NAME = '{1}' AND tc.CONSTRAINT_TYPE = 'PRIMARY KEY'",
            "        ) curr_pk",
            "        FULL JOIN (",
            "            ".concat(pk_column_list),
            "        ) need_pk ON curr_pk.COLUMN_NAME = need_pk.n AND curr_pk.ORDINAL_POSITION = need_pk.p",
            "        WHERE curr_pk.COLUMN_NAME IS NULL OR need_pk.n IS NULL",
            "    ) BEGIN",
            "        RAISERROR('in table {0}.{1} existing primary key does not match the expected', 16, 1)",
            "        RETURN",
            "    END",
        ].join(lib_os.EOL)),
        column_list.map(m => { return  [
            "    IF NOT EXISTS (SELECT TOP 1 COLUMN_NAME FROM INFORMATION_SCHEMA.[COLUMNS] WHERE TABLE_SCHEMA = '{0}' AND TABLE_NAME = '{1}' AND COLUMN_NAME = '".concat(quote(m.name,true),"') BEGIN"),
            "        EXEC ('ALTER TABLE [{0}].[{1}] ADD ".concat(query_column(m), "')"),
            "    END ELSE IF NOT EXISTS (SELECT TOP 1 COLUMN_NAME FROM INFORMATION_SCHEMA.[COLUMNS] WHERE TABLE_SCHEMA = '{0}' AND TABLE_NAME = '{1}' AND COLUMN_NAME = '".concat(quote(m.name,true),"'"),
            "        ".concat("AND DATA_TYPE='", m.type,
                                "' AND IS_NULLABLE = '", (m.nullable === true ? 'YES' : 'NO'),
                                "' AND CHARACTER_MAXIMUM_LENGTH ", (lib_conv.isEmpty(m.len_chars) ? 'IS NULL' : "= ".concat((m.len_chars === 'max' ? "-1" : m.len_chars.toString()))),
                                " AND NUMERIC_PRECISION ", lib_conv.isEmpty(m.precision) ? "IS NULL" : "= ".concat(m.precision.toString()),
                                " AND NUMERIC_SCALE ", lib_conv.isEmpty(m.scale) ? "IS NULL" : "= ".concat(m.scale.toString()), ")"),
            "    BEGIN",
            "        EXEC ('ALTER TABLE [{0}].[{1}] ALTER COLUMN ".concat(query_column(m), "')"),
            "    END"
        ].join(lib_os.EOL)}).join(lib_os.EOL),
        "END",
        descr.go(schema, name, undefined, description),
        column_list.map(m => { return descr.go(schema, name, m.name, m.description) }).join(lib_os.EOL),
    ].filter(f => !lib_conv.isEmpty(f))

    return lib_conv.format(query.join(lib_os.EOL), [quote(schema, true), quote(name, true), quote(description, true)])
}

/**
 * @param {type_column} column
 * @returns {string}
 */
function query_column(column) {
    if (lib_conv.isAbsent(column)) return ''
    let len = ''
    if (!lib_conv.isEmpty(column.len_chars)) {
        len = ''.concat('(', column.len_chars.toString(), ')').toUpperCase()
    } else if (!lib_conv.isEmpty(column.precision) && !lib_conv.isEmpty(column.scale)) {
        len = ''.concat('(', column.precision.toString(), ',', column.scale.toString(), ')')
    }

    let identity = ''
    if (column.identity === true) {
        identity = ' IDENTITY(1,1)'
    }

    return lib_conv.format('[{0}] {1}{2}{3}{4}', [
        quote(column.name, true),
        column.type.toLocaleUpperCase(),
        len,
        identity,
        (column.nullable === true ? ' NULL' : ' NOT NULL')
    ])
}

/**
 * @param {string} schema
 * @param {string} name
 * @param {type_column[]} column_list
 * @returns {string}
 */
function query_pk(schema, name, column_list) {
    let pk_columns = column_list
        .filter(f => !lib_conv.isEmpty(f.pk_position))
        .sort((a, b) => (a.pk_position < b.pk_position ? -1 : 1))
        .map(m => { return lib_conv.format('[{0}] ASC', quote(m.name, true)) })
        .join(',')
    if (lib_conv.isEmpty(pk_columns)) return ''
    return lib_conv.format('CONSTRAINT [PK_{0}_{1}] PRIMARY KEY CLUSTERED ({2}) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]', [
        quote(schema, true),
        quote(name, true),
        pk_columns
    ])
}

// SELECT kcu.COLUMN_NAME, kcu.ORDINAL_POSITION
// FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
// JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu ON kcu.TABLE_SCHEMA =  tc.TABLE_SCHEMA AND kcu.TABLE_NAME =  tc.TABLE_NAME
// WHERE tc.TABLE_SCHEMA = 'aaa' AND tc.TABLE_NAME = 'log' AND tc.CONSTRAINT_TYPE = 'PRIMARY KEY'
