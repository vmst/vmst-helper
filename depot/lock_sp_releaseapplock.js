//@ts-check
const lib_conv = require('viva-convert')
const lib_os = require('os')

exports.go = go

/**
 * exec sp_releaseapplock
 * @param {string} key name lock
 * @param {string} [database] name database
 * @returns {string}
 */
function go (key, database) {
    let key_beauty = lib_conv.toString(key, '')
    if (key_beauty.length <= 0) return undefined

    let exec_path = 'sp_releaseapplock'

    let database_beauty = lib_conv.toString(database, '')
    if (database_beauty.length > 0) {
        exec_path = lib_conv.border_add(database_beauty, '[', ']').concat('..', exec_path)
    }

    return lib_conv.format("EXEC {0} @Resource='{1}', @LockOwner='Session'", [exec_path, key_beauty])
}