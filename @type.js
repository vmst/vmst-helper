// @ts-check
function stub () {}
exports.stub = stub

/**
 * @typedef {'bigint'|'bit'|'decimal'|'int'|'money'|'numeric'|'smallint'|'smallmoney'|'tinyint'|'float'|'real'|'date'|'datetime2'|'datetime'|'datetimeoffset'|'smalldatetime'|'time'|'char'|'text'|'varchar'|'sysname'|'nchar'|'ntext'|'nvarchar'|'binary'|'image'|'varbinary'|'hierarchyid'|'sql_variant'|'xml'|'uniqueidentifier'|'timestamp'|'geometry'|'geography'} sqltype
 */

// /**
//  * @typedef {Object} sqltypename
//  * @property {'bigint'|'bit'|'decimal'|'int'|'money'|'numeric'|'smallint'|'smallmoney'|'tinyint'|'float'|'real'|'date'|'datetime2'|'datetime'|'datetimeoffset'|'smalldatetime'|'time'|'char'|'text'|'varchar'|'sysname'|'nchar'|'ntext'|'nvarchar'|'binary'|'image'|'varbinary'|'hierarchyid'|'sql_variant'|'xml'|'uniqueidentifier'|'timestamp'|'geometry'|'geography'} type
//  */