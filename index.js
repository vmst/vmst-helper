//@ts-check
exports.helper_get_types_sql = require('./helper/get_types_sql.js').go
exports.helper_get_declare = require('./helper/get_declare.js').go
exports.helper_js_to_sql = require('./helper/js_to_sql.js').go

exports.depot_lock_sp_getapplock = require('./depot/lock_sp_getapplock.js').go
exports.depot_lock_sp_releaseapplock = require('./depot/lock_sp_releaseapplock.js').go
exports.depot_sch_description = require('./depot/sch_description.js').go
exports.depot_sch_foreign = require('./depot/sch_foreign.js').go
exports.depot_sch_schema = require('./depot/sch_schema.js').go
exports.depot_sch_table = require('./depot/sch_table.js').go
exports.depot_server_info = require('./depot/server_info.js').go

/**
 * @typedef {import('./depot/sch_table.js').type_column} type_column
 * @typedef {import('./@type.js').sqltype} type_sqltype
 * @typedef {import('./helper/get_types_sql.js').type_sql} type_sql
 */
